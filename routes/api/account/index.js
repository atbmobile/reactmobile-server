'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const router = require('express').Router();
const isAuthenticated = require(APP_BASE + '/middlewares/authenticate')

router.use('/createAccount', require(APP_BASE + '/routes/api/account/createAccount'));
router.use('/getAllAccounts', isAuthenticated, require(APP_BASE + '/routes/api/account/getAllAccounts'));
router.use('/updateAccount', isAuthenticated, require(APP_BASE + '/routes/api/account/updateAccount'));

module.exports = router;
