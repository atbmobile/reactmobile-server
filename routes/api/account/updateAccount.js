//To create accounts for testing
'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const Account = require(APP_BASE + '/models/account');

const findAccountAndUpdate = (req, res, next) => {
    let id = req.body.accountId;
    Account.findAccountAndUpdate(id, req.body, (err, account) => {
           if (err) {
               next(util.getError('updateAccount', httpStatus.INTERNAL_SERVER_ERROR, err, null));
           } else {
               res.json(account);
           }
       });
}

router.route('/').put(findAccountAndUpdate);

module.exports = router;
