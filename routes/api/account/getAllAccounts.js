'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const Account = require(APP_BASE + '/models/account');

const getAllAccounts = (req, res, next)  => {
  const userId = req.MOBILE_PARAMS.userId;
   Account.getAllAccounts(userId, (err, accounts) => {
       if (err) {
         next(util.getError('GetAllAccounts', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
       } else {
           res.json(accounts);
       }
   });
}

router.route('/').get(getAllAccounts);

module.exports = router;
