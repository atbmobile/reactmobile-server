//To create accounts for testing
'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const Account = require(APP_BASE + '/models/account');

const createAccount = (req, res, next) => {
    Account.createAccount(req.body, (err, account) => {
        if (err) {
            if (err.name === 'ValidationError') {
                next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'account'), null);
            } else {
                next(util.getError('CreateAccount', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
            }
        } else {
            res.json({status: httpStatus.OK});
        }
    });
}

router.route('/').post(createAccount);

module.exports = router;
