'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const User = require(APP_BASE + '/models/user');

const createUser = (req, res, next) => {
    User.createUser(req.body, (err, user) => {
        if (err) {
            if (err.name === 'ValidationError') {
                next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'user'), null);
            } else {
                next(util.getError('CreateUser', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
            }
        } else {
            res.json({status: httpStatus.OK});
        }
    });
}

router.route('/').post(createUser);

module.exports = router;