'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const router = require('express').Router();

router.use('/signup', require(APP_BASE + '/routes/api/auth/signup'));
router.use('/signin', require(APP_BASE + '/routes/api/auth/signin'));
router.use('/resetpassword', require(APP_BASE + '/routes/api/auth/resetpassword'));

module.exports = router;