'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const router = require('express').Router();

router.use('/makeTransfer', require(APP_BASE + '/routes/api/transfer/makeTransfer'));

module.exports = router;