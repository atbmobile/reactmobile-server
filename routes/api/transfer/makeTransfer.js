//To create transfer
'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const shortID = require('shortid');
const Account = require(APP_BASE + '/models/account');
const Transaction = require(APP_BASE + '/models/transaction');
const TransactionHandler = require(APP_BASE + '/routes/api/transaction/transactionHandler');
const errors = require('properties-reader')(APP_BASE + '/resources/errors.properties');

//
// function updateAccountAndTransaction(accountNumber, amount,  isFromAccount, referenceNumber){
//
//     console.log("Test :" + accountNumber +" : " + amount + " :: " + referenceNumber);
//     var updatedBalance =0;
//
//     Account.findOne({number: accountNumber},(err, account) =>{
//         if (err){
//             next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, err, null));
//         }
//         else{
//                 console.log(account);
//                 if (isFromAccount) {
//                     referenceNumber = shortID.generate();
//                     updatedBalance = account.currentBalance - amount;
//                 }
//                 else{
//                     updatedBalance = parseFloat(toAccount.currentBalance) + parseFloat(amount);
//                 }
//                 console.log("updatedBalance-->" + updatedBalance);
//                 Account.findOneAndUpdate({number: accountNumber},{$set: {currentBalance: updatedBalance}}, {returnOriginal: false}, (updatedErr, updatedAccount) => {
//                         if (updatedErr) {
//                                 next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, updatedErr, null));
//                         }
//                         console.log("From Account is updated:  "  + accountNumber + " with Amount: "  + updatedAccount);
//
//                         const fromTransaction = new Transaction({
//                             userId : updatedAccount.userId,
//                             referenceNumber: referenceNumber,
//                             accountNumber: updatedAccount.number,
//                             type: updatedAccount.type,
//                             amount: amount
//                         });
//
//
//                         Transaction.createTransaction(fromTransaction, (err, transaction)=> {
//                             if (err) {
//                                 console.log("This is error--> " + err);
//                                 next(util.getError('createTransaction', httpStatus.INTERNAL_SERVER_ERROR, err, null));
//                             } else {
//                                 console.log("This is ----> " + transaction);
//                                 return transaction;
//                             }
//                         });
//                     });
//                 }
//         });
//      return null;
// }

const makeTransfer = (req, res, next) => {
    const fromNumber = req.body.fromNumber;
    const toNumber = req.body.toNumber;
    const fromCurrencyCode = req.body.fromCurrencyCode;
    const toCurrencyCode = req.body.toCurrencyCode;
    const fromType = req.body.fromType;
    const toType = req.body.toType;
    const amount = req.body.amount;

    var updatedFromBalance =0;
    var updatedToBalance =0;
    var referenceNumber =0;

   Account.findOne({number: fromNumber},(err, fromAccount) =>{
        if (err){
            next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, err, null));
        }
        else{
                updatedFromBalance = fromAccount.currentBalance - amount;
                Account.findOneAndUpdate({number: fromNumber},{$set: {currentBalance: updatedFromBalance}}, {returnOriginal: true}, (fromErr, updatedAccount) => {
                if (fromErr) {
                    console.log("----> " +  fromErr);
                    next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, fromErr, null));
                }
                    const fromTransaction = new Transaction({
                        userId : updatedAccount.userId,
                        referenceNumber: shortID.generate(),
                        accountNumber: updatedAccount.number,
                        type: 'Transfer',
                        amount: amount*(-1),
                        description: 'Transfer from Account'
                    });

                    referenceNumber = fromTransaction.referenceNumber;
                    Transaction.createTransaction(fromTransaction, (err, transaction)=> {
                            if (err) {
                                next(util.getError('createTransaction', httpStatus.EXPECTATION_FAILED, err, null));
                            }
                    });
                });
            }
    });


    Account.findOne({number: toNumber},(err, toAccount) =>{
        if (err){
            next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, err, null));
        }
        else{
            updatedToBalance = parseFloat(toAccount.currentBalance) + parseFloat(amount);
            Account.findOneAndUpdate({number: toNumber},{$set: {currentBalance: updatedToBalance}}, {returnOriginal: false}, (toErr, updatedAccount) => {
            if (toErr) {
                next(util.getError('Failed', httpStatus.EXPECTATION_FAILED, toErr, null));
            }

            const toFransaction = new Transaction({
                            userId : updatedAccount.userId,
                            referenceNumber: referenceNumber,
                            accountNumber: updatedAccount.number,
                            type: 'Transfer',
                            amount: amount,
                            description: 'Transfer to Account'
                        });
            Transaction.createTransaction(toFransaction, (err, transaction)=> {

            if (err) {
                next(util.getError('createTransaction', httpStatus.EXPECTATION_FAILED, err, null));
            } else {
                res.json({status: httpStatus.OK,
                          referenceNumber: referenceNumber,
                          fromAccountBalance: updatedFromBalance,
                          toAccountBalance: updatedToBalance

                });
            }
            });
        });
    }
});

}

router.route('/').post(makeTransfer);

module.exports = router;
