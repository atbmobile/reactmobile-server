/**
 * Created by LAE86643 on 8/25/2017.
 */
'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const router = require('express').Router();

router.use('/', require(APP_BASE + '/routes/api/transaction/transactionHandler'));

module.exports = router;
