/**
 * Created by LAE86643 on 8/25/2017.
 */

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const Transaction = require(APP_BASE + '/models/transaction');

const saveTransferTransaction = (req, res, next) => {

    const transactionData = req.body;
    saveTransaction(transactionData, res, next);
}


const saveBillpayTransaction = (req, res, next) => {
    const transactionData = req.body;
    saveTransaction(transactionData, res, next);
}

const saveTransaction = (transactionData, res, next) => {

    if (!transactionData.transactionId) {
        Transaction.createTransaction(transactionData,(err, transaction) => {
            if (err) {
                next(util.getError('createTransaction', httpStatus.INTERNAL_SERVER_ERROR, err, null));
            } else {
                res.json(transaction);
            }
        });
    } else {
        Transaction.updateTransaction(transactionData, (err, transaction) => {
            if (err) {
                next(util.getError('updateTransaction', httpStatus.INTERNAL_SERVER_ERROR, err, null));
            } else {
                res.json(transaction);
            }
        });
    }
}

const getTransaction = (req, res, next) => {
    logger.debug(req.params.accountNumber);
     Transaction.findTransactionByAccountNumber(req.params.accountNumber,(err, transaction) => {
        if (err) {
            next(util.getError('getTransaction', httpStatus.INTERNAL_SERVER_ERROR, err, null));
        } else if (!transaction ) {
            res.json({status: httpStatus.NOT_FOUND});
        } else {
            res.json(transaction);
        }
    });
}

router.route('/:accountNumber').get(getTransaction);
router.route('/transfer').post(saveTransferTransaction);
router.route('/billpay').post(saveBillpayTransaction);

module.exports = router;

