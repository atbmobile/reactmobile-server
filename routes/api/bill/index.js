'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const router = require('express').Router();
const authorize = require(APP_BASE + '/middlewares/authenticate');

router.use('/payee', authorize, require(APP_BASE + '/routes/api/bill/payee'));
router.use('/', authorize, require(APP_BASE + '/routes/api/bill/bill'));

module.exports = router;