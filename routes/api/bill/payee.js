'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const authUtil = require(APP_BASE + '/utils/auth-util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const findUserBy = require(APP_BASE + '/middlewares/find-user-by');
const Model = require(APP_BASE + '/models/payee');

//        logger.debug( 'UserId from Token:' + JSON.stringify( req.MOBILE_PARAMS, null, 4) );

router.route('/').post(
    (req, res, next) => {
        Model.create(req.body, (err, data) => {
            if (err) {
                if (err.name === 'ValidationError') {
                    next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'bill'), null);
                } else {
                    next(util.getError('Create', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                }
            } else {
                res.json({ status: httpStatus.CREATED });
            }
        });
    });

router.route( '/:name?' ).get(
    ( req, res, next ) => {
        let filter = { };
        if ( req.params.name ) filter = { name: { $regex: '.*' + req.params.name + '.*', $options: 'i' } };
        Model.find(
            filter, 'name',
            ( err, docs ) => {
                if ( err ) {
                    next( util.getError( 'Find', httpStatus.INTERNAL_SERVER_ERROR, err, null ), null );
                } else {
                    res.json(docs);
                }
            }
        );
    });

module.exports = router;