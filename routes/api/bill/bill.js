'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const util = require(APP_BASE + '/utils/util');
const authUtil = require(APP_BASE + '/utils/auth-util');
const httpStatus = require('http-status-codes');
const router = require('express').Router();
const findUserBy = require(APP_BASE + '/middlewares/find-user-by');
const Model = require(APP_BASE + '/models/bill');
const Payee = require(APP_BASE + '/models/payee');
const mongoose = require('mongoose');

router.route('/').post(
    (req, res, next) => {
        req.body.userId = req.MOBILE_PARAMS.userId;
        let body = req.body;
        // The payee is defined in the Model as an Object so 
        // I need to instantiate the Payee model with the value received
        body.payee = new Payee( { _id: body.payee } );
        //logger.debug( 'Body:' + JSON.stringify( body, null, 4) );
        Model.create( body, (err, data) => {
            if (err) {
                if (err.name === 'ValidationError') {
                    next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'bill'), null);
                } else {
                    next(util.getError('Create', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                }
            } else {
                const { _id, name, number, payee } = data;
                res.json({ data: { _id, name, number, payee }, status: httpStatus.CREATED});
            }
        });
/*        Payee.findById( body.payee, ( err, data ) => {
            if (err) {
                next(util.getError('Find payee', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
            } else {
                body.payee = data._id;
                Model.create( body, (err, data) => {
                    if (err) {
                        if (err.name === 'ValidationError') {
                            next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'bill'), null);
                        } else {
                            next(util.getError('Create', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                        }
                    } else {
                        const { _id, name, number, payee } = data;
                        res.json({ data: { _id, name, number, payee }, status: httpStatus.CREATED});
                    }
                });
            }
        }); 
*/        //logger.debug( 'Body:' + JSON.stringify( req.body, null, 4) );
});

router.route('/:_id?').get(
    (req, res, next) => {
        const userId = req.MOBILE_PARAMS.userId;
        const id = req.params._id;
        var filter = { userId: userId, deletedAt: null };
        if ( id !== undefined ) filter._id = id;

        Model.find( filter, '_id name number payee' ).populate( 'payee', '_id name' ).exec(
            ( err, docs ) => {
                if (err) {
                    next(util.getError('Find', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                } else {
                    res.json( docs );
                }
            }
        );
});

router.route('/:_id').put(
    (req, res, next) => {
        const userId = req.MOBILE_PARAMS.userId;
        const { name, number } = req.body; 
        Model.findOneAndUpdate( 
            { _id: req.params._id, userId: userId },
            { name, number },
            { runValidators: true },
            ( err, doc ) => {
                if (err) {
                    if (err.name === 'ValidationError') {
                        next(util.getError('Validation', httpStatus.UNPROCESSABLE_ENTITY, err, 'bill'), null);
                    } else {
                        next(util.getError('Create', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                    }
                } else {
                    res.json({ status: httpStatus.OK });
                }
            }
        );    
});

router.route('/:_id').delete(
    (req, res, next) => {
        const userId = req.MOBILE_PARAMS.userId;
        Model.findByIdAndUpdate( 
            req.params._id,
            { deletedAt: new Date },
            ( err, docs ) => {
                if (err) {
                    next(util.getError('Delete', httpStatus.INTERNAL_SERVER_ERROR, err, null), null);
                } else {
                    res.json({status: httpStatus.OK});
                }
            }
        );
});


module.exports = router;