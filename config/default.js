'use strict';

module.exports = {
    SERVER: {
        HOST: 'localhost',
        PORT: 8081,
        CORS_WHITELIST: [
            'http://localhost',
            'http://127.0.0.1'
        ]
    },
    DB: {
        HOST: 'localhost',
        PORT: 27017,
        DATABASE: 'ReactMobile',
        URL: 'mongodb://localhost:27017/reactmobile'
    },
    LOG: {
        DIR: process.env.NODE_PATH + '/logs',
        FILE_LEVEL : 'info',
        CONSOLE_LEVEL : 'info',
        ACCESS_LOG_FORMAT : ':remote-addr :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :response-time :res[content-length]',
    },
    JWT: {
        SECRET: 'elibomtcaer',
        PRIVATE_KEY: '37LvDSm4XvjYOh9Y',
        // TOKEN_EXPIRY: '24 hours',
        TOKEN_EXPIRY: 86400    // 24 hours in seconds
    },
    NODE_ENV : process.env.NODE_ENV || 'development',
};
