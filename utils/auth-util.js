'use strict';

const APP_BASE = process.env.NODE_PATH;
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const config = require(APP_BASE + '/config');
const util = require(APP_BASE + '/utils/util');
const httpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');

exports.createToken = (tokenData) => {
    tokenData.exp = Math.floor(Date.now() / 1000) + config.JWT.TOKEN_EXPIRY;
    return jwt.sign(tokenData, config.JWT.PRIVATE_KEY);
}

exports.verifyToken = (token, next, callback) => {
    jwt.verify(token, config.JWT.PRIVATE_KEY, (err, decodedToken) => {
        if (err) {
            if (err.name === 'TokenExpiredError') {
                next(util.getError('TokenExpired', httpStatus.UNAUTHORIZED, err, null));
            } else if (err.name === 'JsonWebTokenError') {
                next(util.getError('InvalidToken', httpStatus.UNAUTHORIZED, err, null));
            } else {
                next(util.getError('VerifyToken', httpStatus.INTERNAL_SERVER_ERROR, err, null));
            }
        } else {
            callback(decodedToken);
        }
    });
}
