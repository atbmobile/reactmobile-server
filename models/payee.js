'use strict';

const APP_BASE = process.env.NODE_PATH;
const config = require(APP_BASE + '/config');
const util = require(APP_BASE + '/utils/util');
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const errors = require('properties-reader')(APP_BASE + '/resources/errors.properties');

logger.debug('Initializing model.payee');

const mongoose = require('mongoose');
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError  = mongoose.Error.ValidatorError;
const Schema = mongoose.Schema;

const Payee = new Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: [true, errors.get('validation.payee.name.required')],
        minlength: [2, errors.get('validation.payee.name.minlength')],
        maxlength: [100, errors.get('validation.payee.name.maxlength')],
        trim: true
    },
    description: {
        type:String,
        trim: true
    }
}, {timestamps: true} );

const autoIncrement = require(APP_BASE +'/db/db').autoIncrement;
Payee.plugin(autoIncrement.plugin, {
    model: 'payee',
    field: '_id',
    startAt: 1
});

const payee = mongoose.model( 'Payee', Payee );

module.exports = payee;

