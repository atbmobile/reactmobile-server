/**
 * Created by LAE86643 on 8/25/2017.
 */
'use strict';

const APP_BASE = process.env.NODE_PATH;
const config = require(APP_BASE + '/config');
const util = require(APP_BASE + '/utils/util');
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const errors = require('properties-reader')(APP_BASE + '/resources/errors.properties');

logger.debug('Initializing model.transaction');

const mongoose = require('mongoose');
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError  = mongoose.Error.ValidatorError;
const Schema = mongoose.Schema;

const Transaction = new Schema({
    userId: {
        type: Number,
        required: [true, errors.get('validation.account.userId.required')]
    },
    referenceNumber: {
        type: String,
        required: true
    },
    accountNumber:{
        type: String,
        required: [true, errors.get('validation.account.number.required')],
        trim: true
    },
    status: {
        type: String,
        enum: {
            values: ['Pending','Completed','Canceled'],
            message: errors.get('validation.transaction.status.match')
        },
        default: 'Pending'

    },
    type: {
        type: String,
        required: true,
        enum: {
            values: ['Deposit', 'Withdraw', 'Transfer', 'Billpay'],
            message: errors.get('validation.transaction.type.match')
        }
    },
    amount: {
        type: Number,
        required: true
    },
    description : String
  },  {timestamps: true});


const uniqueValidator = require(APP_BASE +'/db/db').uniqueValidator;
Transaction.plugin(uniqueValidator);

Transaction.statics = {
    createTransaction: function(requestData, callback) {
        this.create(requestData, callback);
    },

    updateTransaction: function(transaction, callback) {
        this.findOneAndUpdate({'_id': transaction.transactionId},  {'$set': transaction.data},{'new': true}, callback );
    },

    findTransaction: function(query, callback) {
        this.find(query, callback);
    },

    findTransactionByAccountNumber: function(accountNumber,callback) {
        this.find({accountNumber: accountNumber}, null , {sort: {createdAt:'desc'}} ,callback);
    }
}

const transaction = mongoose.model('transaction', Transaction);

module.exports = transaction;


