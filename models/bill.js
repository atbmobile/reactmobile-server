'use strict';

const APP_BASE = process.env.NODE_PATH;
const config = require(APP_BASE + '/config');
const util = require(APP_BASE + '/utils/util');
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const errors = require('properties-reader')(APP_BASE + '/resources/errors.properties');
//const Payee = require(APP_BASE + '/models/payee');

logger.debug('Initializing model.bill');

const mongoose = require('mongoose');
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError  = mongoose.Error.ValidatorError;
const Schema = mongoose.Schema;

const Bill = new Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: [true, errors.get('validation.bill.name.required')],
        minlength: [5, errors.get('validation.bill.name.minlength')],
        maxlength: [30, errors.get('validation.bill.name.maxlength')],
        trim: true
    },
    payee: {
        //type: String,
        // TODO - Make this property an object so that it can be easily referenced
        type: Schema.Types.ObjectId,
        ref: 'Payee',
        required: [true, errors.get('validation.bill.payee.required')],
        trim: true
    },
    number: {
        type: String,
        required: [true, errors.get('validation.bill.number.required')]
    },
    userId: {
        type: String
    },
    deletedAt: {
        type: Date
    }
}, {timestamps: true} );

const autoIncrement = require( APP_BASE +'/db/db' ).autoIncrement;
Bill.plugin(autoIncrement.plugin, {
    model: 'bill',
    field: '_id',
    startAt: 1
});

const bill = mongoose.model( 'Bill', Bill );

module.exports = bill;

