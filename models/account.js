'use strict';

const APP_BASE = process.env.NODE_PATH;
const config = require(APP_BASE + '/config');
const util = require(APP_BASE + '/utils/util');
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const errors = require('properties-reader')(APP_BASE + '/resources/errors.properties');

logger.debug('Initializing model.account');

const mongoose = require('mongoose');
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError  = mongoose.Error.ValidatorError;
const Schema = mongoose.Schema;

const Account = new Schema({
    name: {
        type: String,
        required: [true, errors.get('validation.account.name.required')],
        minlength: [2, errors.get('validation.account.name.minlength')],
        maxlength: [30, errors.get('validation.account.name.maxlength')],
        trim: true
    },
    nickname: {
        type: String,
        required: false,
        minlength: [2, errors.get('validation.account.nickname.minlength')],
        maxlength: [30, errors.get('validation.account.nickname.maxlength')],
        trim: true
    },
    isFavorite: {
      type: Boolean,
      default: false
    },
    number: {
        type: String,
        unique:true,
        required: [true, errors.get('validation.account.number.required')],
        trim: true,
    },
    type: {
        type: String,
        enum: {
            values: ['Banking', 'Investment', 'Credit Card', 'Loan'],
            message: errors.get('validation.account.type.match')
        },
        required: [true, errors.get('validation.account.type.required')]
    },
    currentBalance: {
        type: Number,
        required: [true, errors.get('validation.account.currentBalance.required')]
    },
    availableBalance: {
        type: Number
    },
    currencyCode: {
        type: String,
        minlength: [3, errors.get('validation.account.currencyCode.length')],
        maxlength: [3, errors.get('validation.account.currencyCode.length')],
        required: [true, errors.get('validation.account.currencyCode.required')]
    },
    userId: {
        type: Number,
        required: [true, errors.get('validation.account.userId.required')]
    }
}, {timestamps: true});


const uniqueValidator = require(APP_BASE +'/db/db').uniqueValidator;
Account.plugin(uniqueValidator);

const autoIncrement = require(APP_BASE +'/db/db').autoIncrement;
Account.plugin(autoIncrement.plugin, {
    model: 'account',
    field: '_id',
    startAt: 1
});

Account.statics = {
    //find all accounts by user
    getAllAccounts: function(userId, callback) {
      this.find({ userId: userId } , callback);
    },

    //added to create accounts for testing
    createAccount: function(requestData, callback) {
        this.create(requestData, callback);
    },

    //to update favflag or nickname
    findAccountAndUpdate: function(id, account, callback) {
        this.findOneAndUpdate({ _id: id}, {'$set': account}, {'new': true}, callback);
    },

}

const account = mongoose.model('account', Account);

module.exports = account;
