'use strict';

process.env.NODE_PATH = __dirname;
const APP_BASE = process.env.NODE_PATH;
const config = require(APP_BASE + '/config');
const logger = require(APP_BASE + '/utils/logger')(module.filename);
const cluster = require('cluster');
const cpuCount = config.NODE_ENV !== 'development' ? require('os').cpus().length : 1;

if (cluster.isMaster) {
    logger.info('Starting ReactMobile API server cluster.');
    for (let i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
    cluster.on('exit', function(server) {
        logger.info('Server ' + server.id + ' died, restoring');
        cluster.fork();
    });
} else {
    require(APP_BASE + '/server/server');
}
